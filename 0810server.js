const express=require('express');
const app=express();
// decimos que los bodys por defecto nos los parcheen asi
app.use(express.json());

const port=process.env.PORT ||3000;
app.listen(port);

console.log("API escuchando en el puerto BIP BIP " + port);

app.get("/apitechu/v1/hello",

function(req,res){
  console.log("GET /apitechu/v1/hello");
  res.send('{"msg":"Hola desde API TechU!"}');
}

)



app.get("/apitechu/v1/users",
function (req, res){
  // console.log("GET /apitechu/v1/users");
  //  res.sendFile ('usuarios.json', {root: __dirname});
  // otra forma de hacer lo anterior, lo mismo es la siguiente forma
  var users=require('./usuarios.json');


var Objetoresultado={};


if (req.query.$count == "true") {
  console.log("Se filtra por count y el valor es...");
  console.log(req.query.$count);
  Objetoresultado.count=users.length;
};


if (req.query.$top) {
  console.log("Se filtra por top y el valor es....");
  console.log(req.query.$top);
  Objetoresultado.users = users.slice(0,req.query.$top);
  res.send(Objetoresultado);

} else {
    console.log("No se filtra por top y devuelvo todos los usuarios")
    Objetoresultado.users=users;
    res.send(Objetoresultado);
  };
}

)


// nuevo el lunes

app.post("/apitechu/v1/users",
function (req, res){
  console.log("entramos en el post post/apitechu/v1/");
  // Si queremos ver que llega desde las cabeceras
  // console.log(req.headers);
  // si solo queremos algun valor de la cabecera:
  console.log(req.headers.first_name);
  console.log(req.headers.last_name);
  console.log(req.headers.email);
// definimos una nueva variable newUser
  var newUser = {
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "email" : req.headers.email,
  }
  var users = require ("./usuarios.json");
  // al ser un array es push
  users.push(newUser);

  //llamamos a la funcion definida mas abajo y se ejecuta e en users que es el fichero usuarios.json
writeUserDataToFile(users);

  res.send("usuario añadido con exito");

}
)


app.delete("/apitechu/v1/users/:id",

function(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La id enviada es: " + req.params.id);

var users = require("./usuarios.json");
users.splice(req.params.id -1,1);
writeUserDataToFile(users);
console.log("usuario borrado");
}
)


// nueva url de REFERENCIA con todas las opciones
app.post("/apitechu/v1/monstruo/:p1/:p2",

function(req,res){
  console.log("POST /apitechu/v1/monstruo/:p1/:p2");

  console.log("parametros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);

  // res.send('{"msg":"Hola desde API TechU!"}');
}

)



// nueva funcion nueva, a parte. Data es genérico, puede tener cualquier nombre, lo mismo que hay en JSON.stringify pero nada mas!!

function writeUserDataToFile(data){

const fs=require('fs');

var jsonUserData = JSON.stringify(data);

fs.writeFile("./usuarios.json", jsonUserData, "utf8",

function (err){
  if (err){
    console.log(err);
  }else{
    console.log("datos escritos en el fichero");
  }

}

) ;

}
